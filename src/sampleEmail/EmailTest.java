package sampleEmail;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author padmahasa.narayana
 *
 */
public class EmailTest {

	static Properties props;
	static Properties propertiesFile;
	static String recipientsString;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Configuring mail client with SMTP properties");
		propertiesFile = new Properties();

		try {
			FileInputStream fileInputStream = new FileInputStream("./config.properties");
			propertiesFile.load(fileInputStream);
		} catch (Exception e) {
			System.out.println("Exception caught while reading filel " + e);
			e.printStackTrace();
		}
		final String username = propertiesFile.getProperty("username");
		final String password = propertiesFile.getProperty("password");

		props = new Properties();
		props.put("mail.smtp.auth", propertiesFile.getProperty("mail.smtp.auth"));
		if (propertiesFile.getProperty("mail.smtp.ssl.trust") != null) {
			props.put("mail.smtp.ssl.trust", propertiesFile.getProperty("mail.smtp.ssl.trust"));
		}
		props.put("mail.smtp.starttls.enable", propertiesFile.getProperty("mail.smtp.starttls.enable"));
		props.put("mail.smtp.host", propertiesFile.getProperty("mail.smtp.host"));
		props.put("mail.smtp.port", propertiesFile.getProperty("mail.smtp.port"));
		Message message = null;

		System.out.println("Configured SMTP to use ");
		System.out.println("SMTP host = " + props.getProperty("mail.smtp.host"));
		System.out.println("Port No = " + props.getProperty("mail.smtp.port"));
		System.out.println("User name = " + username);

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

//		session.setDebug(true);

		InetAddress ip;
		String hostname;
		try {
			ip = InetAddress.getLocalHost();
			hostname = ip.getHostName();
			System.out.println("Your current IP address : " + ip);
			System.out.println("Your current Hostname : " + hostname);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		try {
			EmailTest emailTest = new EmailTest();
			message = emailTest.sendToSampleEmailAddressOrGroup(message, session, username);
			Transport.send(message);

			System.out.println(
					"Email notification to " + recipientsString + " email address has been sent successfully.");
		} catch (MessagingException mex) {
			System.out.println("Exception caught inside EmailNotificationUtil -> main method");
			mex.printStackTrace();
		}
	}

	public Message sendToSampleEmailAddressOrGroup(Message message, Session session, String username) {
		try {
			message = new MimeMessage(session);
			try {
				message.setFrom(
						new InternetAddress(username, propertiesFile.getProperty("messageConfig.from.personal")));
				message.addFrom(
						new InternetAddress[] { new InternetAddress(propertiesFile.getProperty("messageConfig.addFrom"),
								propertiesFile.getProperty("messageConfig.addFrom.personal")) });
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			recipientsString = propertiesFile.getProperty("messageConfig.recipients");
			InternetAddress[] toAddress = null;
			if (recipientsString.contains(",")) {
				List<String> listOfToAddressString = Arrays.asList(recipientsString.split(","));
				List<InternetAddress> listOfInternetAddressToAddress = new ArrayList<>();
				for (String address : listOfToAddressString) {
					listOfInternetAddressToAddress.add(new InternetAddress(address));
				}
				if (listOfInternetAddressToAddress != null && !listOfInternetAddressToAddress.isEmpty()) {
					toAddress = listOfInternetAddressToAddress.toArray(new InternetAddress[listOfInternetAddressToAddress.size()]);
				}
			}
			else {
				toAddress = new InternetAddress[1];
				toAddress[0] = new InternetAddress(recipientsString);
			}

			try {
				message.setRecipients(Message.RecipientType.TO, toAddress);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			message.setSubject(propertiesFile.getProperty("messageConfig.subject"));
			message.setContent(propertiesFile.getProperty("messageConfig.content"), "text/html");
		} catch (MessagingException mex) {
			System.out
					.println("Exception caught inside EmailNotificationUtil -> sendToSampleEmailAddressOrGroup method");
			mex.printStackTrace();
		}
		return message;
	}

}
